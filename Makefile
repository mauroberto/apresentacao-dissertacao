#sudo apt-get install texlive-full

FILE=demo

$(FILE).pdf: clear $(FILE).tex demo.bib sty/*.sty
	pdflatex $(FILE).tex
	bibtex $(FILE).aux
	
clear:
	rm -f $(FILE).pdf
